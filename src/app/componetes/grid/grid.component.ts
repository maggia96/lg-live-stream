import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser'
@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss'],
})
export class GridComponent implements OnInit {
  items: Array<string>;

  constructor() {
    this.items = ['uno', 'dos', 'tres', 'cuatro', 'cinco', 'seis', 'siete', 'ocho', 'nueve'];
  }


  ngOnInit() { }

}
