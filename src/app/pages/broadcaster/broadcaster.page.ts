import { Component, OnInit } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';


const APPLICATION_ID:string = 'C48LqWjW9xm5Zfxtwhkcbw';

@Component({
  selector: 'app-broadcaster',
  templateUrl: './broadcaster.page.html',
  styleUrls: ['./broadcaster.page.scss'],
})
export class BroadcasterPage implements OnInit {

  isBroadcasting = false;
  isPending = false;
  broadcaster: any;
  errListenerId = false;

  // Application id generated at https://dashboard.bambuser.com/developer
  
  constructor(private toastCtrl: ToastController ,   public platform: Platform) { 
    platform.ready().then(() => {
      // Using array syntax workaround, since types are not declared.
      
      if (window['bambuser']) {
        this.broadcaster = window['bambuser']['broadcaster'];
        this.broadcaster.setApplicationId(APPLICATION_ID);
        console.log(window['bambuser'] , "uno");
      } else {
        // Cordova plugin not installed or running in a web browser
        console.log(window['bambuser'] , "dos");
      }
    });
  }

  ngOnInit() {
  }

 

 async ionViewDidLoad() {
    console.log('Starting viewfinder');

  if (!this.broadcaster) {
    await new Promise(resolve => setTimeout(resolve, 500)); // Let page animations to finish
    alert('broadcaster is not ready yet');
    return;
  }

  this.broadcaster.showViewfinderBehindWebView();
  document.getElementsByTagName('body')[0].classList.add("show-viewfinder");
  }

  ionViewWillLeave() {
    console.log('Removing viewfinder');
    document.getElementsByTagName('body')[0].classList.remove("show-viewfinder");
    if (this.broadcaster) {
      this.broadcaster.hideViewfinder();
    }
  }
  async start() {
    if (this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Starting broadcast...',
      position: 'middle',
    });
    (await toast).present();
  
    console.log('Starting broadcast');
    await this.broadcaster.startBroadcast();
  
    (await toast).dismiss();
    this.isBroadcasting = true;
    this.isPending = false;
  }

  async stop() {
    if (!this.isBroadcasting || this.isPending) return;
    this.isPending = true;
    const toast = this.toastCtrl.create({
      message: 'Ending broadcast...',
      position: 'middle'
    });
    (await toast).present();
  
    console.log('Ending broadcast');
    await this.broadcaster.stopBroadcast();
  
    (await toast).dismiss();
    this.isBroadcasting = false;
    this.isPending = false;
  }
}


