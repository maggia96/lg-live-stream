import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GridComponent } from '../componetes/grid/grid.component';

import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path:'grid',
    component: GridComponent
  },
  {
    path: 'call_user/:id',
    loadChildren: () => import('../peerjs/peerjs.module').then( m => m.PeerjsPageModule)
  },
  {
    path: 'receive-video-call/:id',
    loadChildren: () => import('../receive-video-call/receive-video-call.module').then( m => m.ReceiveVideoCallPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
