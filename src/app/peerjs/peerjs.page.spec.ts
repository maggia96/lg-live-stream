import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PeerjsPage } from './peerjs.page';

describe('PeerjsPage', () => {
  let component: PeerjsPage;
  let fixture: ComponentFixture<PeerjsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeerjsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PeerjsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
