import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PeerjsPageRoutingModule } from './peerjs-routing.module';

import { PeerjsPage } from './peerjs.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PeerjsPageRoutingModule
  ],
  declarations: [PeerjsPage]
})
export class PeerjsPageModule {}
