import { Component, ElementRef, OnInit } from '@angular/core';
import { PeerjsService } from '../peerjs.service';
import { ActivatedRoute, Params } from '@angular/router'
import { ThrowStmt } from '@angular/compiler';
@Component({
  selector: 'app-peerjs',
  templateUrl: './peerjs.page.html',
  styleUrls: ['./peerjs.page.scss'],
})
export class PeerjsPage implements OnInit {

  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;
  finish_call:boolean;
  

  
  constructor( public webRTC: PeerjsService, public elRef: ElementRef, private rutaActiva: ActivatedRoute) { 
    this.userId=this.rutaActiva.snapshot.params.id;
    this.finish_call=false;
  
  }

  ngOnInit(){
    this.init();
  }

  init() {
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    this.webRTC.init(this.userId, this.myEl, this.partnerEl);
    this.hide_finish_call_button();
  }

  
  call() {
    console.log(this.partnerId , "este es el id en el ts");
    this.webRTC.call(this.partnerId);
    this.swapVideo('my-video');
  }

  swapVideo(topVideo: string) {
    this.topVideoFrame = topVideo;
  }

  finishCall(){
   this.webRTC.endCall();
   this.hide_finish_call_button();
  }

  hide_finish_call_button(){
    setTimeout(() => {
      if(this.webRTC.myStream){
        this.finish_call = true;
      }else{
        this.finish_call=false;
      }
    }, 1000);
  }
}


