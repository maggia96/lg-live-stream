import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReceiveVideoCallPage } from './receive-video-call.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiveVideoCallPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReceiveVideoCallPageRoutingModule {}
