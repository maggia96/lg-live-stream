import { Component, ElementRef, OnInit } from '@angular/core';
import { PeerjsService } from '../peerjs.service';
import { ActivatedRoute, Params } from '@angular/router'
@Component({
  selector: 'app-receive-video-call',
  templateUrl: './receive-video-call.page.html',
  styleUrls: ['./receive-video-call.page.scss'],
})
export class ReceiveVideoCallPage implements OnInit {

  topVideoFrame = 'partner-video';
  userId: string;
  partnerId: string;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;
  finish_call:boolean;
  
  
  constructor(public webRTC: PeerjsService,public elRef: ElementRef,private rutaActiva: ActivatedRoute) {
    this.userId=this.rutaActiva.snapshot.params.id;
    this.finish_call=false;
  }

  ngOnInit() {
    this.startCamara();
  }

  ngOnDestroy(){
    console.log("se esta destruyendo"); 
  }

  init() {
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    this.webRTC.init(this.userId, this.myEl, this.partnerEl);
    this.hide_finish_call_button();
  }

  startCamara(){
    this.myEl = this.elRef.nativeElement.querySelector('#my-video');
    this.partnerEl = this.elRef.nativeElement.querySelector('#partner-video');
    this.webRTC.asign_media_elements(this.myEl , this.partnerEl);
    this.webRTC.getMedia();
  }

  
  call() {
    console.log(this.partnerId , "este es el id en el ts");
    this.webRTC.call(this.partnerId);
    this.swapVideo('my-video');
  }

  finishCall(){
    this.webRTC.endCall();
    this.hide_finish_call_button();
  }
  
  swapVideo(topVideo: string) {
    this.topVideoFrame = topVideo;
  }

  hide_finish_call_button(){
    setTimeout(() => {
      if(this.webRTC.myStream){
        this.finish_call = true;
      }else{
        this.finish_call=false;
      }
    }, 1000);
  }
}


