import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReceiveVideoCallPageRoutingModule } from './receive-video-call-routing.module';

import { ReceiveVideoCallPage } from './receive-video-call.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReceiveVideoCallPageRoutingModule
  ],
  declarations: [ReceiveVideoCallPage]
})
export class ReceiveVideoCallPageModule {}
