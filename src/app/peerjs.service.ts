
import { HostListener, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import Peer from 'peerjs';

@Injectable({
  providedIn: 'root'
})
export class PeerjsService {

  peer: Peer;
  myStream: MediaStream;
  myEl: HTMLMediaElement;
  partnerEl: HTMLMediaElement;
  mediaConnection: Peer.MediaConnection;
  options: Peer.PeerJSOption;
  user_name:string;

  constructor(private root:Router) {  
    this.options = {  // not used, by default it'll use peerjs server
      key: 'cd1ft79ro8g833di',
      debug: 3,
      host: 'mdevz.com',
      port: 34000,
      path: '/',
     
    };
  }

  
  asign_media_elements(myEl: HTMLMediaElement, partnerEl: HTMLMediaElement){
    this.myEl = myEl;
    this.partnerEl = partnerEl;
  }

  getMedia() {
    navigator.getUserMedia({ audio: true, video: true }, (stream) => {
      console.log("obteniendo media camara microfono succes");
      this.handleSuccess(stream);
      console.log(stream , "stream");
    },(error) => {
      this.handleError(error);
    });
  }

  async init(userId: string, myEl: HTMLMediaElement, partnerEl: HTMLMediaElement) {
    this.myEl = myEl;
    this.partnerEl = partnerEl;
    try {
      this.getMedia();
      console.log("media");
    } 
    catch (e) {
      console.log("error al crear encontrar media" , e);
      this.handleError(e);
    }
    
    await this.createPeer(userId);
   
  }

  async createPeer(userId: string) {
    this.peer = new Peer(userId , this.options);
    console.log(this.peer , "creando el peer");
    this.peer.on('open', () => {
      console.log("se conecto");
      this.wait();
    });
    this.peer.on('error', (err:any)=> { 
      this.callErrorHandler(err);
      console.log(err.type , "aqui esta el error");
    });
    this.peer.on('close' , function(){
      console.log("se a cerrado la llamada");
     
    });
    this.peer.on('disconnected' , function(){
      console.log("se a descoenctado");
      
    });
  }

  call(partnerId: string) {
    console.log(partnerId , "este es el id");
    const call = this.peer.call(partnerId, this.myStream);
    call.on('stream', (stream) => {
      console.log(stream,"");
      this.partnerEl.srcObject = stream;
      this.asign_user_name(partnerId);
    });
    this.ListenCallState(call);
  }

  endCall(){
    if(this.peer != null){
      this.peer.disconnect();
      this.peer.destroy();
    }
    this.partnerEl.srcObject = null;
    this.myStream = null;
    this.myEl.srcObject = null;
    this.user_name="";
    this.root.navigate(["/home"]);
  }
  
  wait() {
    this.peer.on('call', (call) => {
      console.log("llamada entrando");
      call.answer(this.myStream);
      console.log(call.answer , "here i am");
      call.on('stream', (stream) => {
        console.log("on stream");
        //this.asign_user_name(call.peer);
        this.partnerEl.srcObject = stream;
      });
      this.ListenCallState(call);
    });
    
   }

  asign_user_name(id){
    console.log(id , "id recibido");
    if(id == 1){
      this.user_name="juan";
      return;
    }
    else if(id == 2){
      this.user_name="pedro";
      return
    }
    else{
      this.user_name="luis";
    }

  }
   
  handleSuccess(stream: MediaStream) {
    this.myStream = stream;
    this.myEl.srcObject = stream;
  }

  handleError(error: any) {
    console.log(error , "este es el error ahhora");
    console.log(error.message , "este es el error ahhorax2");
    if(error.message === "Requested device not found") {
      alert("Los dispositivos solicitados no fueron encontrados");
     this.endCall();
    }
    if(error.message === "Permission denied") {
      alert("permiso denegado para acceder a la camara");
      this.endCall();
      
    }
    if(error.message === "Could not start video source") {
      alert("No se pudo inicar la camara");
      this.endCall();
      
    }
    else if (error.name === 'ConstraintNotSatisfiedError') {
      //const v = constraints.video;
     // this.errorMsg(`The resolution ${v.width.exact}x${v.height.exact} px is not supported by your device.`);
      this.errorMsg(`The resolution px is not supported by your device.`);
    } else if (error.name === 'PermissionDeniedError') {
      this.errorMsg('Permissions have not been granted to use your camera and ' +
        'microphone, you need to allow the page access to your devices in ' +
        'order for the demo to work.');
    }
    this.errorMsg(`getUserMedia error: ${error.message}`, error);
    
  }

  errorMsg(msg: string, error?: any) {
    const errorElement = document.querySelector('#errorMsg');
    errorElement.innerHTML += `<p>${msg}</p>`;
    if (typeof error !== 'undefined') {
      console.error(error.name);
    }
  }
  
  callErrorHandler(error:any){
   
    switch(error.type) { 
      case "peer-unavailable": { 
        alert("no esta conectado el usuario que intenta llamar.");
        this.endCall();
         break; 
      } 
      case 'browser-incompatible': { 
        alert("Su navegador no es compatible.");
        this.endCall();
         break; 
      }
      case 'disconnected': { 
        alert("Usted ya se ha desconectado y no puede volver a conectarse");
        this.endCall();
         break; 
      }
      case 'invalid-id': { 
        alert("El id pasado contiene caracteres invalidos");
        this.endCall();
        break; 
      } 
      case 'invalid-key': { 
       console.log("The API key passed into the Peer constructor contains illegal characters or is not in the system (cloud server only).");
       this.endCall();
        break; 
      } 
      case 'network': { 
        alert("No se ha podido establecer una conexión con el servidor , verifique su conexion a internet");
        this.endCall();
        break; 
      } 
      case 'server-error': { 
        alert("no es posible alcanzar el servidor");
        this.endCall();
        break; 
      } 
      case 'ssl-unavailable': { 
      console.log("PeerJS is being used securely, but the cloud server does not support SSL. Use a custom PeerServer.");
        this.endCall();
        break; 
      } 
      case 'socket-error': { 
      console.log("An error from the underlying socket.");
        this.endCall();
        break; 
      } 
      case 'socket-closed': { 
        console.log("The underlying socket closed unexpectedly.");
          this.endCall();
          break; 
        } 
      case 'unavailable-id': { 
        alert("el Id ingresado ya existe");
        this.endCall();
        break; 
      } 
      case 'webrtc': { 
        console.log("Native WebRTC errors.");
        this.endCall();
        break; 
      } 
      default: { 
        alert("ocurrio un error. Porfavor vuelva a intentarlo.");
        console.log(error);
        this.endCall();
        break; 
      } 
    } 
  }
  
  ListenCallState(call){
    (call as any)._negotiator.connection.on('iceStateChanged' , (state) =>{ 
        
      if(state === "disconnected"){
        this.endCall();
        alert("La otra persona se a desconectado");
      }
      if(state === "connected"){
       this.asign_user_name(call.peer);
       
      }
    }); 
  }

}
